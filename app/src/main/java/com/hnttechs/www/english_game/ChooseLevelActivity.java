package com.hnttechs.www.english_game;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by dell on 1/13/16.
 */
public class ChooseLevelActivity extends ActionBarActivity{

    GridView gdv_choose_level;
    static String sceneTitle;
    static final String[ ] GRID_LEVEL = new String[] {
            "Arendelle Castle","The Adventure","Ice Palace"
    };
    SQLiteDatabase db;
    static CustomArrayList level_arrayList = new CustomArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_level);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = openOrCreateDatabase("Game_Result", 0, null); // database need to open to use database in this activity.

        LinearLayout layout_activity_choose_level = (LinearLayout)findViewById(R.id.layout_activity_choose_level);

        // select level which have score =10 to handle lock and unlock to level button.
        android.database.Cursor cursor = db.rawQuery("SELECT * FROM tb_Score where Score = 10", null);
        while (cursor.moveToNext()) {
            String level = cursor.getString(cursor.getColumnIndex("Level_no"));
            level_arrayList.add(level);
        }

        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView title = (TextView)findViewById(R.id.title);
        title.setTypeface(tf_title);
        title.setText("Choose Level");

        Intent i = getIntent();
        sceneTitle = i.getStringExtra("SceneTitle");
        gdv_choose_level = (GridView) findViewById(R.id.gdv_choose_level);
        gdv_choose_level.setAdapter(new ChooseLevelGridAdapter(this, GRID_LEVEL,level_arrayList));

        if(sceneTitle.equals("Frozen")) {layout_activity_choose_level.setBackgroundResource(R.drawable.frozen_background);}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
