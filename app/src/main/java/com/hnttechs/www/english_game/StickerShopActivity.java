package com.hnttechs.www.english_game;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 3/19/16.
 */
public class StickerShopActivity extends ActionBarActivity {

    ListView listview;
    ListViewAdapter adapter;
    SQLiteDatabase db;
    static final Integer[ ] GRID_LEVEL = new Integer[] {
            1,2,3,4,5,6
    };

    List<Integer> own_point = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sticker_shop);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView title = (TextView)findViewById(R.id.txt_title);
        title.setTypeface(tf_title);
        title.setText("Sticker Shop");

        db = openOrCreateDatabase("Game_Result", 0, null);

        listview = (ListView) findViewById(R.id.mylist);
        TextView txt_own_point = (TextView)findViewById(R.id.txt_own_points);

        // select total point which player was own.
        String count = "SELECT count(*) FROM tb_Point";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0)
        {
            select_own_point();
            txt_own_point.setText("You have " + own_point.get(0).toString() + " Points");

            adapter = new ListViewAdapter(StickerShopActivity.this, GRID_LEVEL,own_point.get(0));

        } else {

            txt_own_point.setText("You have no points");
            adapter = new ListViewAdapter(StickerShopActivity.this, GRID_LEVEL,0);
        }


        listview.setAdapter(adapter);
    }


    private List<Integer> select_own_point()
    {

        int position=0;

        Cursor c  = db.rawQuery("SELECT * FROM tb_Point", null);

        if (c != null) {
            while (c.moveToNext()) {
                own_point.add(position, c.getInt(c.getColumnIndex("Point")));
                position++;
            }
        }
        return own_point;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
