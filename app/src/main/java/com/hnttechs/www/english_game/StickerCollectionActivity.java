package com.hnttechs.www.english_game;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.GridView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 3/19/16.
 */
public class StickerCollectionActivity extends ActionBarActivity {

    GridView gdv_sticker_collection;
    SQLiteDatabase db;
    List<Integer> STICKERNO = new ArrayList<Integer>();
    List<String> POINTS = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticker_collection);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = openOrCreateDatabase("Game_Result", 0, null);

        select_StickerNo();
        select_Points();

        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView title = (TextView)findViewById(R.id.txt_title);
        title.setTypeface(tf_title);
        title.setText("Sticker Collection");

        // show all sticker which player was own.
        gdv_sticker_collection = (GridView) findViewById(R.id.gdv_sticker_collection);
        gdv_sticker_collection.setAdapter(new StickerCollection_GridViewAdapter(this, STICKERNO, POINTS));
    }

    private List<Integer> select_StickerNo()
    {

        int position=0;

        Cursor c  = db.rawQuery("SELECT StickerNo FROM tb_Sticker", null);

        if (c != null) {
            while (c.moveToNext()) {

                STICKERNO.add(position, c.getInt(c.getColumnIndex("StickerNo")));
                position++;
            }
        }
        return STICKERNO;
    }
    private List<String> select_Points()
    {

        int position=0;

        Cursor cur  = db.rawQuery("SELECT Points FROM tb_Sticker", null);

        if (cur != null) {
            while (cur.moveToNext()) {

                POINTS.add(position, cur.getString(cur.getColumnIndex("Points")));
                position++;
            }
        }
        return POINTS;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
