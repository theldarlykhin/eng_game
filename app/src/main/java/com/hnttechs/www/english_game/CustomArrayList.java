package com.hnttechs.www.english_game;

import java.util.ArrayList;

/**
 * Created by dell on 3/25/16.
 */
public class CustomArrayList extends ArrayList<String> {

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public int indexOf(Object o) {
        int size = this.size();
        if (o == null) {
            for (int i = 0; i < size ; i++) {
                if (this.get(i) == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size ; i++) {
                if (this.get(i).contains(String.valueOf(o))) {
                    return i;
                }
            }
        }
        return -1;
    }


}
