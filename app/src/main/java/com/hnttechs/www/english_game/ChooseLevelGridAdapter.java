package com.hnttechs.www.english_game;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

/**
 * Created by dell on 1/25/16.
 */
public class ChooseLevelGridAdapter extends BaseAdapter {

    private Context context;
    private final String[] gridValues;
    private CustomArrayList level;
    static Button btn_level;
    public ChooseLevelGridAdapter(Context context, String[] gridValues, CustomArrayList level_array) {
        this.context        = context;
        this.gridValues     = gridValues;
        this.level = level_array;
    }

    @Override
    public int getCount() { return gridValues.length; }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate( R.layout.choose_level_grid_item , null);
            btn_level = (Button) gridView
                        .findViewById(R.id.btn_level);

            if(ChooseLevelActivity.sceneTitle.equals("Frozen")) {btn_level.setBackgroundResource(R.drawable.frozen_level_button);}


            btn_level.setText(gridValues[position].toString());
            btn_level.setId(position);


            for(int j= 0; j<level.size(); j++) {
                if(level.contains("Arendelle Castle")) {
                    if(btn_level.getText().equals("The Adventure")) {
                        btn_level.setEnabled(true);
                        btn_level.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent i = new Intent(context, ActivityWatchMovie.class);
                                i.putExtra("SceneLevel", ChooseLevelActivity.sceneTitle.toString());
                                i.putExtra("SelectedLevel","The Adventure");
                                i.putExtra("TotalLevel", gridValues.length);
                                i.putExtra("QuestionNo", 0);
                                context.startActivity(i);
                            }
                        });
                    }
                }
                if(level.contains("The Adventure")) {
                    if(btn_level.getText().equals("Ice Palace")) {
                        btn_level.setEnabled(true);
                        btn_level.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent i = new Intent(context, ActivityWatchMovie.class);
                                i.putExtra("SceneLevel", ChooseLevelActivity.sceneTitle.toString());
                                i.putExtra("SelectedLevel", "Ice Palace");
                                i.putExtra("TotalLevel", gridValues.length);
                                i.putExtra("QuestionNo", 0);
                                context.startActivity(i);
                            }
                        });
                    }
                }
            }

            if(btn_level.getId()==0) {
                btn_level.setEnabled(true);
                btn_level.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(context, ActivityWatchMovie.class);
                        i.putExtra("SceneLevel", ChooseLevelActivity.sceneTitle.toString());
                        i.putExtra("SelectedLevel", "Arendelle Castle");
                        i.putExtra("TotalLevel", gridValues.length);
                        i.putExtra("QuestionNo", 0);
                        context.startActivity(i);
                    }
                });
            }


        } else { gridView = (View) convertView; }

        return gridView;
    }
}
