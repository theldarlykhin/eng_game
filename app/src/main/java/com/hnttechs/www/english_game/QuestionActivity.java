package com.hnttechs.www.english_game;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by dell on 1/20/16.
 */
public class QuestionActivity extends ActionBarActivity {
    String selectedLevel;
    Integer questionNo;
    String sceneLevel;
    Integer totalLevel;
    ProgressDialog mProgressDialog;
    private RadioGroup rdoGroupAnswer;
    private RadioButton rdoAnswer;
    static JSONArray json_array_question;
    static JSONArray json_array_real_answer;
    static JSONArray json_array_answer;
    static String json_qanda;
    static TextView txt_question;
    static String real_answer;
    SQLiteDatabase db;
    static Integer score_record = 0;
    static Integer point = 0;
    static String play_date;
    List<Integer> own_point = new ArrayList<Integer>();
    static CustomArrayList movie_arrayList = new CustomArrayList();
    static CustomArrayList level_arrayList = new CustomArrayList();
    static List<Integer> score_arrayList = new ArrayList<Integer>();
    RadioButton rdo_three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_question);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        sceneLevel = i.getStringExtra("SceneLevel");
        selectedLevel = i.getStringExtra("SelectedLevel");
        totalLevel = i.getIntExtra("TotalLevel", 0);
        questionNo = i.getIntExtra("QuestionNo", 0);

        db = openOrCreateDatabase("Game_Result", 0, null);

        // select json file from assets folder here.
        // qanda.json file saved question and answer data for quiz.
        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(getAssets().open(
                    "qanda.json")));
            String temp;
            while ((temp = br.readLine()) != null)
                sb.append(temp);
        } catch (IOException e) {
            e.printStackTrace();
        }
        json_qanda = sb.toString();

        txt_question = (TextView) findViewById(R.id.txt_question);
        ImageView btn_answer = (ImageView) findViewById(R.id.btn_answer);
        rdoGroupAnswer = (RadioGroup) findViewById(R.id.rdoGroupAnswer);
        rdo_three = (RadioButton)findViewById(R.id.rdoAnswerThree);

        new pullJSON().execute();

        ImageView btn_replay = (ImageView) findViewById(R.id.btn_replay);
        btn_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getBaseContext(), MoviePlayActivity.class);
                i.putExtra("SceneLevel", sceneLevel);
                i.putExtra("SelectedLevel", selectedLevel);
                i.putExtra("TotalLevel", totalLevel);
                i.putExtra("QuestionNo", questionNo);
                startActivity(i);
            }
        });

        btn_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int selectedId = rdoGroupAnswer.getCheckedRadioButtonId();
                rdoAnswer = (RadioButton) findViewById(selectedId);

                if (rdoAnswer.getText().toString().equals(real_answer)) { // check the answer is correct or not.

                    score_record++;
                    point = point + 10;

                    Calendar c = Calendar.getInstance();
                    DateFormat df = new SimpleDateFormat("MM/dd/yy");
                    play_date = df.format(new Date());

                    // when user finish all question, this code will run.
                    if (questionNo == 9) {
                        android.database.Cursor cursor = db.rawQuery("SELECT * FROM tb_Score", null);

                        while (cursor.moveToNext()) {
                            String movie = cursor.getString(cursor.getColumnIndex("Movie"));
                            String level = cursor.getString(cursor.getColumnIndex("Level_no"));
                            movie_arrayList.add(movie);
                            level_arrayList.add(level);
                        }

                        // save score to table.
                        select_score();
                        if (movie_arrayList.contains(sceneLevel)) {
                            if (level_arrayList.contains(selectedLevel)) {

                                for (int j =0;j<score_arrayList.size();j++) {
                                    if (score_record >score_arrayList.get(j)) {
                                        update_Score(score_record,sceneLevel,selectedLevel);
                                    }
                                }
                            } else {

                                insert_Result(play_date, sceneLevel, score_record, selectedLevel);
                            }
                        } else {

                            insert_Result(play_date, sceneLevel, score_record, selectedLevel);
                        }

                        select_own_point();
                        if(own_point.isEmpty()==true) {
                            insert_Point(point);
                        } else {
                            update_Point(own_point.get(0) + point);
                        }


                        if(selectedLevel.equals("Ice Palace")) {    // when user finish all level. this code will run.
                            Intent i = new Intent(getApplicationContext(), Activity_congrats.class);
                            i.putExtra("SceneLevel", sceneLevel);
                            startActivity(i);
                            score_record = 0;
                            point = 0;
                            finish();
                        } else if(selectedLevel.equals("The Adventure") && score_record==10) {
                            Intent i = new Intent(getApplicationContext(), ScoreBoard.class);
                            i.putExtra("SceneLevel", sceneLevel);
                            startActivity(i);
                            score_record = 0;
                            point = 0;
                            finish();
                        }
                        else {

                            Intent i = new Intent(getApplicationContext(), ScoreBoard.class);
                            i.putExtra("SceneLevel", sceneLevel);
                            startActivity(i);
                            score_record = 0;
                            point = 0;
                            finish();
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), MoviePlayActivity.class);
                        i.putExtra("SceneLevel", sceneLevel);
                        i.putExtra("SelectedLevel", selectedLevel);
                        i.putExtra("QuestionNo", questionNo + 1);
                        startActivity(i);
                        finish();
                    }

                } else {

                    // when player have clicked wrong anser, this code will run.
                    final AlertDialog alertDialog = new AlertDialog.Builder(QuestionActivity.this).create();
                    alertDialog.setTitle("Wrong Answer");
                    alertDialog.setMessage("Oops! You selected the wrong answer. Try again next time.");
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();

                            Calendar c = Calendar.getInstance();
                            DateFormat df = new SimpleDateFormat("MM/dd/yy");
                            play_date = df.format(new Date());

                            if (questionNo == 9) {
                                android.database.Cursor cursor = db.rawQuery("SELECT * FROM tb_Score", null);

                                CustomArrayList movie_arrayList = new CustomArrayList();
                                CustomArrayList level_arrayList = new CustomArrayList();
                                while (cursor.moveToNext()) {
                                    String movie = cursor.getString(cursor.getColumnIndex("Movie"));
                                    String level = cursor.getString(cursor.getColumnIndex("Level_no"));
                                    movie_arrayList.add(movie);
                                    level_arrayList.add(level);
                                }

                                if (movie_arrayList.contains(sceneLevel)) {
                                    if (level_arrayList.contains(selectedLevel)) {
                                    } else {
                                        insert_Result(play_date, sceneLevel, score_record, selectedLevel);
                                    }
                                } else {
                                    insert_Result(play_date, sceneLevel, score_record, selectedLevel);
                                }

                                select_own_point();
                                if(own_point.isEmpty()==true) {
                                    insert_Point(point);
                                } else {
                                    insert_Point(own_point.get(0)+point);
                                }

                                Intent i = new Intent(getApplicationContext(), ScoreBoard.class);
                                i.putExtra("SceneLevel", sceneLevel);
                                startActivity(i);
                                score_record = 0;
                                point = 0;
                                finish();
                            } else {
                                Intent i = new Intent(getApplicationContext(), MoviePlayActivity.class);
                                i.putExtra("SceneLevel", sceneLevel);
                                i.putExtra("SelectedLevel", selectedLevel);
                                i.putExtra("QuestionNo", questionNo + 1);
                                startActivity(i);
                                finish();
                            }

                        }
                    });
                    alertDialog.show();
                }

            }
        });
    }

    private void insert_Result(String PlayDate, String name, Integer score, String levelNo) {
        try {
            db.execSQL("Insert into tb_Score (Play_date,Movie,Score,Level_no)values('" + PlayDate + "','" + name.toString() + "'," + score + ",'" + levelNo + "');");


        } catch (SQLException e) {
            setTitle("exception");
        }
    }

    private void insert_Point(Integer point) {
        try {
            db.execSQL("insert into tb_Point (Point) values (" + point + ")");

        } catch (SQLException e) {
            setTitle("exception");
        }
    }


    private void update_Point(Integer point) {
        try {
            db.execSQL("UPDATE tb_Point SET Point=" + point);

        } catch (SQLException e) {
            setTitle("exception");
        }
    }

    private void update_Score(Integer score, String scene, String level) {
        try {
            db.execSQL("UPDATE tb_Score SET Score=" + score + " WHERE Movie= '" + scene + "' and Level_no = '" +level+"';");

        } catch (SQLException e) {
            setTitle("exception");
        }
    }

    private List<Integer> select_own_point()
    {

        int position=0;

        Cursor c  = db.rawQuery("SELECT * FROM tb_Point", null);

        if (c != null) {
            while (c.moveToNext()) {
                own_point.add(position, c.getInt(c.getColumnIndex("Point")));
                position++;
            }
        }
        return own_point;
    }


    private List<Integer> select_score()
    {

        int position=0;

        Cursor c  = db.rawQuery("SELECT * FROM tb_Score", null);

        if (c != null) {
            while (c.moveToNext()) {
                score_arrayList.add(position, c.getInt(c.getColumnIndex("Score")));
                position++;
            }
        }
        return score_arrayList;
    }

    // select question and answer data from json here.
    private class pullJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(QuestionActivity.this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage("loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject jsonObjMain = new JSONObject(json_qanda);

                json_array_question = jsonObjMain.getJSONArray("Question");
                json_array_real_answer = jsonObjMain.getJSONArray("RealAnser");
                json_array_answer = jsonObjMain.getJSONArray("Answer");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            mProgressDialog.dismiss();
            try {
                if(selectedLevel.equals("Arendelle Castle")) {
                    txt_question.setText(json_array_question.getJSONArray(0).get(questionNo).toString());

                    JSONArray json_answers = json_array_answer.getJSONArray(0).getJSONArray(questionNo);
                    real_answer = json_array_real_answer.getJSONArray(0).get(questionNo).toString();

                    for (int j = 0; j < rdoGroupAnswer.getChildCount(); j++) {
                        ((RadioButton) rdoGroupAnswer.getChildAt(j)).setText(json_answers.get(j).toString());
                    }
                } else if(selectedLevel.equals("The Adventure")) {
                    rdo_three.setVisibility(View.VISIBLE);
                    txt_question.setText(json_array_question.getJSONArray(1).get(questionNo).toString());

                    JSONArray json_answers = json_array_answer.getJSONArray(1).getJSONArray(questionNo);
                    real_answer = json_array_real_answer.getJSONArray(1).get(questionNo).toString();

                    for (int j = 0; j < rdoGroupAnswer.getChildCount(); j++) {
                        ((RadioButton) rdoGroupAnswer.getChildAt(j)).setText(json_answers.get(j).toString());
                    }
                } else if(selectedLevel.equals("Ice Palace")) {
                    txt_question.setText(json_array_question.getJSONArray(2).get(questionNo).toString());

                    JSONArray json_answers = json_array_answer.getJSONArray(2).getJSONArray(questionNo);
                    real_answer = json_array_real_answer.getJSONArray(2).get(questionNo).toString();

                    for (int j = 0; j < rdoGroupAnswer.getChildCount(); j++) {
                        ((RadioButton) rdoGroupAnswer.getChildAt(j)).setText(json_answers.get(j).toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
