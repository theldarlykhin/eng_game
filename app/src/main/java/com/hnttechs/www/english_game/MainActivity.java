package com.hnttechs.www.english_game;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView game_name_title = (TextView)findViewById(R.id.game_name_title);
        game_name_title.setTypeface(tf_title);
        game_name_title.setText("Learn English With Disney");

        // create database here. This app used three tables as follow. tb_Score table use to store
        // players scores. tb_Point table use to store points. tb_Sticker use to store bought sticker.
        db = openOrCreateDatabase("Game_Result", 0, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS tb_Score (_id integer PRIMARY KEY autoincrement,Play_date String, Movie text, Score Integer,Level_no text)");
        db.execSQL("CREATE TABLE IF NOT EXISTS tb_Point (_id integer PRIMARY KEY autoincrement,Point Integer)");
        db.execSQL("CREATE TABLE IF NOT EXISTS tb_Sticker (_id integer PRIMARY KEY autoincrement,StickerNo Integer,Points text)");

        ImageView img_play = (ImageView) findViewById(R.id.btn_play);
        img_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MovieSceneActivity.class);
                startActivity(i);

                if (db.isOpen()) {
                    db.close();
                }
            }
        });

        ImageView img_score = (ImageView)findViewById(R.id.btn_score);
        img_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ScoreBoard.class);
                startActivity(i);
                if (db.isOpen()) {
                    db.close();
                }
            }
        });

        ImageView btn_sticker_shop = (ImageView) findViewById(R.id.btn_sticker_shop);
        btn_sticker_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StickerShopActivity.class);
                startActivity(i);

                if (db.isOpen())
                {
                    db.close();
                }
            }
        });

        ImageView btn_sticker_collection = (ImageView)findViewById(R.id.btn_sticker_collection);
        btn_sticker_collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StickerCollectionActivity.class);
                startActivity(i);
                if (db.isOpen())
                {
                    db.close();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
