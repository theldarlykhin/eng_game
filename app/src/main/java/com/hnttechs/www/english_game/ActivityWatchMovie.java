package com.hnttechs.www.english_game;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dell on 5/2/16.
 */
public class ActivityWatchMovie extends ActionBarActivity {

    String selectedLevel;
    Integer totalLevel;
    String scenelevel;
    Integer questionNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.watch_movie);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        selectedLevel = i.getStringExtra("SelectedLevel");
        totalLevel = i.getIntExtra("TotalLevel", 0);
        scenelevel = i.getStringExtra("SceneLevel");
        questionNo=i.getIntExtra("QuestionNo", 0);

        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView title = (TextView)findViewById(R.id.title);
        title.setTypeface(tf_title);
        title.setText("Watch movie carefully.");

        final ImageView btn_play = (ImageView)findViewById(R.id.btn_play);
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getBaseContext(),MoviePlayActivity.class);
                i.putExtra("SceneLevel",ChooseLevelActivity.sceneTitle.toString());
                i.putExtra("SelectedLevel", selectedLevel);
                i.putExtra("TotalLevel",totalLevel);
                i.putExtra("QuestionNo",0);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
