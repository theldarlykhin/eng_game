package com.hnttechs.www.english_game;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.util.List;

/**
 * Created by dell on 7/15/15.
 */
public class StickerCollection_GridViewAdapter extends BaseAdapter {

    private Context context;
    private final List<Integer> sticker_no;
    private final List<String> points;

    public StickerCollection_GridViewAdapter(Context context, List<Integer> sticker_no, List<String> points) {
        this.context        = context;
        this.sticker_no     = sticker_no;
        this.points         = points;
    }

    @Override
    public int getCount() { return sticker_no.size(); }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {

        View gridView;
        if(convertView==null)
        {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = li.inflate( R.layout.sticker_grid_item , null);
        }else{
            gridView = convertView;
        }

            ImageView img_sticker = (ImageView) gridView
                    .findViewById(R.id.img_sticker);

            if(sticker_no.get(position)==1) {
                img_sticker.setImageResource(R.drawable.sticker_1);}
            else if(sticker_no.get(position)==2) {
                img_sticker.setImageResource(R.drawable.sticker_2);}
            else if(sticker_no.get(position)==3) {
                img_sticker.setImageResource(R.drawable.sticker_3);}
            else if(sticker_no.get(position)==4) {
                img_sticker.setImageResource(R.drawable.sticker_4);}
            else if(sticker_no.get(position)==5) {
                img_sticker.setImageResource(R.drawable.sticker_5);}
            else if(sticker_no.get(position)==6) {
                img_sticker.setImageResource(R.drawable.sticker_6);}
            else if(sticker_no.get(position)==7) {
                img_sticker.setImageResource(R.drawable.sticker_7);}
            else if(sticker_no.get(position)==8) {
                img_sticker.setImageResource(R.drawable.sticker_8);}



        return gridView;
    }

}
