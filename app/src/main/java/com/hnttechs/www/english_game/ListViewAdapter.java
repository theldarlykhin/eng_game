package com.hnttechs.www.english_game;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapter extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    static Integer[] stickerid;

    ImageView img_sticker;
    TextView txt_points;
    ImageView btn_collect;
    SQLiteDatabase db;

    String Points;
    static Integer ownpoint;
    Integer sticker_price;

    public ListViewAdapter(Context context, Integer[] sticker_id,Integer own_point) {
        this.context = context;
        stickerid = sticker_id;
        ownpoint = own_point;
    }

    @Override
    public int getCount() {
        return stickerid.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final TextView shop_name;
        TextView address;
        TextView phone_no;

        db = context.openOrCreateDatabase("Game_Result", 0, null);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sticker_item, parent, false);
        }

        img_sticker = (ImageView)convertView.findViewById(R.id.img_sticker);
        txt_points = (TextView)convertView.findViewById(R.id.points);
        btn_collect = (ImageView) convertView.findViewById(R.id.btn_collect);

        if(stickerid[position]==1)        {  img_sticker.setImageResource(R.drawable.sticker_1); txt_points.setText("30 Points"); sticker_price=30; }
        else if (stickerid[position]==2)  {  img_sticker.setImageResource(R.drawable.sticker_2); txt_points.setText("100 Points"); sticker_price=100;}
        else if(stickerid[position]==3)   {  img_sticker.setImageResource(R.drawable.sticker_3); txt_points.setText("500 Points"); sticker_price=500;}
        else if (stickerid[position]==4)  {  img_sticker.setImageResource(R.drawable.sticker_4); txt_points.setText("250 Points"); sticker_price=250;}
        else if(stickerid[position]==5)   {  img_sticker.setImageResource(R.drawable.sticker_5); txt_points.setText("750 Points"); sticker_price=750;}
        else if (stickerid[position]==6)  {  img_sticker.setImageResource(R.drawable.sticker_6); txt_points.setText("100 Points"); sticker_price=100;}

        if(txt_points.getText().equals("30 Points")) {
            btn_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // check user's point was enough to buy this sticker or not.
                    if (30 < ownpoint) {
                        Points = txt_points.getText().toString();
                        insert_Result(stickerid[position], Points);
                        update_Point(ownpoint-30);
                        Intent i = new Intent(context, StickerCollectionActivity.class);
                        context.startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Not enough point.");
                        builder.setMessage("You don't have enough point to buy this sticker.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        } else if(txt_points.getText().equals("500 Points")) {
            btn_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (100 < ownpoint) {
                        Points = txt_points.getText().toString();
                        insert_Result(stickerid[position], Points);
                        update_Point(ownpoint - 100);
                        Intent i = new Intent(context, StickerCollectionActivity.class);
                        context.startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Not enough point.");
                        builder.setMessage("You don't have enough point to buy this sticker.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        } else if(txt_points.getText().equals("100 Points")) {
            btn_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (500 < ownpoint) {
                        Points = txt_points.getText().toString();
                        insert_Result(stickerid[position], Points);
                        update_Point(ownpoint - 500);
                        Intent i = new Intent(context, StickerCollectionActivity.class);
                        context.startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Not enough point.");
                        builder.setMessage("You don't have enough point to buy this sticker.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        } else if(txt_points.getText().equals("100 Points")) {
            btn_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (250 < ownpoint) {
                        Points = txt_points.getText().toString();
                        insert_Result(stickerid[position], Points);
                        update_Point(ownpoint - 250);
                        Intent i = new Intent(context, StickerCollectionActivity.class);
                        context.startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Not enough point.");
                        builder.setMessage("You don't have enough point to buy this sticker.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        } else if(txt_points.getText().equals("125 Points")) {
            btn_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (750 < ownpoint) {
                        Points = txt_points.getText().toString();
                        insert_Result(stickerid[position], Points);
                        update_Point(ownpoint - 750);
                        Intent i = new Intent(context, StickerCollectionActivity.class);
                        context.startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Not enough point.");
                        builder.setMessage("You don't have enough point to buy this sticker.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        } else if(txt_points.getText().equals("60 Points")) {
            btn_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (100 < ownpoint) {
                        Points = txt_points.getText().toString();
                        insert_Result(stickerid[position], Points);
                        update_Point(ownpoint - 100);
                        Intent i = new Intent(context, StickerCollectionActivity.class);
                        context.startActivity(i);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Not enough point.");
                        builder.setMessage("You don't have enough point to buy this sticker.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        }
        return convertView;
    }

    private void update_Point(Integer point) {
        try {
            db.execSQL("UPDATE tb_Point SET Point=" + point);

        } catch (SQLException e) {
        }
    }




    private void insert_Result(Integer stickerid,String points) {
        try {
            db.execSQL("Insert into tb_Sticker (StickerNo,Points)values(" + stickerid + ",'"  + points.toString() + "');");

        } catch (SQLException e) { }
    }

}