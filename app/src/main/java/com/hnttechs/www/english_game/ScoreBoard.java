package com.hnttechs.www.english_game;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 2/9/16.
 */
public class ScoreBoard extends ActionBarActivity {

    SQLiteDatabase db;
    ListView mylist;
    String sceneLevel;
    static List<Integer> score_arrayList = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView title = (TextView)findViewById(R.id.title);
        title.setTypeface(tf_title);
        title.setText("Score Board");


        db = openOrCreateDatabase("Game_Result", 0, null);

        Intent i = getIntent();
        sceneLevel = i.getStringExtra("SceneLevel");

        select_Result();
        if (db.isOpen())
        {
            db.close();
        }

        Button btn_home = (Button)findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        Button btn_next = (Button)findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), ChooseLevelActivity.class);
                i.putExtra("SceneTitle", sceneLevel);
                startActivity(i);
            }
        });
    }

    // set score data to scoreboard listview.
    private void select_Result()
    {
        mylist = (ListView)findViewById(R.id.mylist);
        View view = View.inflate(getBaseContext(), R.layout.header, null);

        mylist.addHeaderView(view);
        android.database.Cursor cursor = db.rawQuery("SELECT * FROM tb_Score", null);
        String[] from = {"Play_date", "Movie", "Level_no", "Score"};
        int[] to = new int[] {R.id.text1, R.id.text2, R.id.text3, R.id.text4};
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(getBaseContext(), R.layout.four_column_list_item, cursor, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        cursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if (view.getId() == R.id.text4) {
                    int getIndex = cursor.getColumnIndex("Score");
                    int score = cursor.getInt(getIndex);
                    TextView scoretextview = (TextView) view;
                    scoretextview.setText(score + "/10");
                    return true;
                }
                return false;
            }
        });
        mylist.setAdapter(cursorAdapter);


        /*  The last two parameters in SimpleCursorAdapter constructor are the "from" and "to" parameters:
            We want to get the name of each book which is stored in the column name title, this is where we get the information "from".
            Next we need to tell it where "to" go: android.R.id.text1 is a TextView in the android.R.layout.simple_list_item_1 layout. (You can dig through your SDK and see the simple_list_item_1.xml file yourself or just trust me for the moment.)
            Now both the "from" and "to" parameters are arrays because we can pass more than one column of information, try this adapter as well:*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
