package com.hnttechs.www.english_game;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dell on 7/15/15.
 */
public class GridViewAdapter extends BaseAdapter {

    private Context context;
    private final String[] gridValues;

    public GridViewAdapter(Context context, String[] gridValues) {
        this.context        = context;
        this.gridValues     = gridValues;
    }

    @Override
    public int getCount() { return gridValues.length; }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;


        Typeface tf_title = Typeface.createFromAsset(context.getAssets(), "fonts/penguinattack.ttf");

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate( R.layout.grid_item , null); //inflate grid item to gdv_movie_scene here.

            TextView txt_movie_title = (TextView)gridView.findViewById(R.id.txt_movie_title);
            txt_movie_title.setTypeface(tf_title);
            txt_movie_title.setText(gridValues[position]);
            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.img_movie_scene);

            String arrLabel = gridValues[ position ];
            if (arrLabel.equals("Frozen"))       { imageView.setImageResource(R.drawable.frozen); }
            else if (arrLabel.equals("Inside Out"))  { imageView.setImageResource(R.drawable.insideout); }
            else if (arrLabel.equals("BayMax"))    { imageView.setImageResource(R.drawable.baymax); }
            else if (arrLabel.equals("How to Train Your dragon")) { imageView.setImageResource(R.drawable.dragon); }
            else if (arrLabel.equals("Cinderella"))   { imageView.setImageResource(R.drawable.cindrelle); }
        } else { gridView = (View) convertView; }

        return gridView;
    }
}
