package com.hnttechs.www.english_game;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.hnttechs.www.english_game.AnimatedGifImageView.TYPE;

/**
 * Created by dell on 1/20/16.
 */
public class MoviePlayActivity extends ActionBarActivity {

    private AnimatedGifImageView animatedGifImageView;
    boolean switchMe = false;
    String selectedLevel;
    Integer totalLevel;
    String scenelevel;
    Integer questionNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent i = getIntent();
        selectedLevel = i.getStringExtra("SelectedLevel");
        totalLevel = i.getIntExtra("TotalLevel", 0);
        scenelevel = i.getStringExtra("SceneLevel");
        questionNo=i.getIntExtra("QuestionNo", 0);


        animatedGifImageView = ((AnimatedGifImageView)findViewById(R.id.animatedGifImageView));

        if(scenelevel.toString().equals("Frozen")) { animatedGifImageView.setImageResource(R.drawable.frozen_big); }

        // this code will handle to run gif file.
        switchMe = false;
                if (switchMe==false) {
                    switchMe = true;
                    if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==0)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_1,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==1)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_2,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==2)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_3,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==3)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_4,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==4)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_5,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==5)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_6,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==6)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_7,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==7)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_8,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==8)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_9,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Arendelle Castle") &&
                            questionNo==9)
                    {animatedGifImageView.setAnimatedGif(R.raw.arendelle_10,
                            TYPE.STREACH_TO_FIT);}

                    if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==0)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_1,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==1)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_2,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==2)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_3,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==3)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_4,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==4)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_5,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==5)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_6,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==6)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_7,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==7)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_8,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==8)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_9,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("The Adventure") &&
                            questionNo==9)
                    {animatedGifImageView.setAnimatedGif(R.raw.adventure_10,
                            TYPE.STREACH_TO_FIT);}

                    if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==0)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_1,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==1)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_2,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==2)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_3,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==3)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_4,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==4)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_5,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==5)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_6,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==6)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_7,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==7)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_8,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==8)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_9,
                            TYPE.STREACH_TO_FIT);}
                    else if(scenelevel.toString().equals("Frozen") &&
                            selectedLevel.equals("Ice Palace") &&
                            questionNo==9)
                    {animatedGifImageView.setAnimatedGif(R.raw.ice_place_10,
                            TYPE.STREACH_TO_FIT);}
                }


        ImageView btn_question = (ImageView)findViewById(R.id.btn_question);
        btn_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), QuestionActivity.class);
                i.putExtra("SceneLevel", scenelevel);
                i.putExtra("SelectedLevel", selectedLevel);
                i.putExtra("TotalLevel",totalLevel);
                i.putExtra("QuestionNo",questionNo);
                startActivity(i);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
