package com.hnttechs.www.english_game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

/**
 * Created by dell on 1/13/16.
 */
public class MovieSceneActivity extends ActionBarActivity{

    GridView gdv_movie_scene;
    // GRID_DATA array have movie scene name which can play in this app.
    static final String[ ] GRID_DATA = new String[] {
            "Frozen",
            "Inside Out",
            "BayMax",
            "How to Train Your dragon",
            "Cinderella"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_scene);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Back arrow from menu bar is setting here.

        // change textview's font style here.
        Typeface tf_title = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/penguinattack.ttf");
        TextView title = (TextView)findViewById(R.id.lbl_title);
        title.setTypeface(tf_title);
        title.setText("Choose Movie to Play");

        gdv_movie_scene = (GridView) findViewById(R.id.gdv_movie_scene); //gridview initialize here.
        gdv_movie_scene.setAdapter(new GridViewAdapter(this, GRID_DATA)); // set data to gridview.
        gdv_movie_scene.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id) {

                        //click event when player choose Frozen from gdv_movie_scene.
                        if(GRID_DATA[position].equals("Frozen")) {
                            Intent i = new Intent(getApplicationContext(), WalkthroughActivity.class); // walkthroughactivity is tutorial activity.
                            i.putExtra("SceneTitle", GRID_DATA[position].toString()); // send parameters to walkthroughactivity.
                            startActivity(i);
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
